
***********
* README: *
***********

DESCRIPTION:
------------
This module provides an Turkish public identification (T.C. Kimlik No) number field type for CCK.


REQUIREMENTS:
-------------
The tckimlikno.module requires the content.module to be installed.
optional modules: token


INSTALLATION:
-------------
1. Place the entire email directory into your Drupal sites/all/modules/
   directory.

2. Enable the email module by navigating to:

     administer > modules


Features:
---------
  * validation of T.C. identification number
  * provides Tokens

Note:
-----


Author:
-------
Guray CELIK
otomobul@gmail.com